<?php

namespace App\Providers;

use App\Events\SendMailPaymentEvent;
use App\Mail\SendMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendMailPaymentListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendMailPaymentEvent  $event
     * @return void
     */
    public function handle(SendMailPaymentEvent $event)
    {
        $client = $event->client;        
        $payment = $event->payment;
        $payment->status = 'paid';
        $payment->save();        
        Mail::to($client->email)->send(new SendMail($client));        
    }
}
