<?php

namespace App\Events;

use App\Client;
use App\Payment;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class SendMailPaymentEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $client;
    public $payment;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Client $client, Payment $payment)
    {
        $this->client = $client;
        $this->payment = $payment;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
