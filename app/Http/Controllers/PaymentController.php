<?php

namespace App\Http\Controllers;

use App\Payment;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Jobs\CreateAndSendPayment;

class PaymentController extends Controller
{
    public function getAllData() {
        if (isset($_GET["client"])) {
            return Payment::where('user_id', $_GET["client"])->get();
        } 
        return Payment::all();
    }

    public function store(Request $request) {
        $data = $request->all();
        
        $payment = Payment::create([            
            "payment_date" => null,
            "expires_at" => date('Y-m-d', strtotime(date('Y-m-d') . ' +30 day')),
            "status" => "pending",
            "user_id" => $data['id_client'],
            "clp_usd" => null
        ]);        

        CreateAndSendPayment::dispatch($payment)->onQueue('payment');

        return $payment;
    }
}
