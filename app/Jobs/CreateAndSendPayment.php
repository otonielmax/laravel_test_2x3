<?php

namespace App\Jobs;

use App\Client;
use App\Events\SendMailPaymentEvent;
use App\Payment;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CreateAndSendPayment implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $payment;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Payment $payment)
    {
        $this->payment = $payment;        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {        
        $today = date('Y-m-d');
        $todayFormat = date('d-m-Y');        
        $paymentToday = Payment::where('payment_date', $today)->get();
        $client = Client::findOrFail($this->payment->user_id);                        
        
        // Load Payment amount from database
        if (isset($paymentToday) && $paymentToday->count() > 0) {
            $this->payment->clp_usd = $paymentToday[0]->clp_usd;
        }
        // Load Payment amount from api
        else {
            $apiUrl = 'https://mindicador.cl/api/dolar/'.$todayFormat;

            if ( ini_get('allow_url_fopen') ) {
                $json = file_get_contents($apiUrl);
            } else {                
                $curl = curl_init($apiUrl);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                $json = curl_exec($curl);
                curl_close($curl);
            }
            
            $dailyIndicators = json_decode($json);              
            if (isset($dailyIndicators->serie) && count($dailyIndicators->serie) > 0) {
                $this->payment->clp_usd = $dailyIndicators->serie[0]->valor;
            }
        }        

        // Update payment date
        $this->payment->payment_date = $today;
        $this->payment->save();

        // Send Mail
        event(new SendMailPaymentEvent($client, $this->payment));
    }
}
