# Test Laravel 2x3

Proyecto Laravel en la version 6.0

## Comenzando 🚀

Clona con git en el directorio local de tu preferencia con el siguiente comando:

```
git clone https://gitlab.com/otonielmax/laravel_test_2x3.git
```

### Pre-requisitos 📋

Recuerda tener composer instalado y actualizado, en caso de que asi no sea dirigete al siguiente enlace para descargarlo:

```
https://getcomposer.org/download/
```

Para actualizar abre tu terminal o consola y ejecuta:

```
composer update
```

### Instalación 🔧

Despues de haber clonado el repositorio en el directorio de tu preferencia, procederemos con la instalación de todo lo necesario para correr el proyecto en nuestro entorno, recuerda abrir tu terminal y ubicarte en el directorio raiz del repositorio clonado en tu maquina.

_1: Instalar paquetes_

```
composer install
```

_2: Generar keys de Laravel_

```
php artisan key:generate
```

_3: Configurar tu proyecto local_

```
Dirigete a .env y modifica las variables para manejo de base de datos y queue (Para la prueba se uso database como driver)

DB_CONNECTION={tu gestor ejemplo: mysql, sqlite, etc}
DB_HOST={tu direccion ip o dominio ejemplo: 127.0.0.1, localhost, etc}
DB_PORT={puerto de conexion a tu db ejemplo: 3306, 5432, etc}
DB_DATABASE={nombre de tu base de datos}
DB_USERNAME={usuario}
DB_PASSWORD={clave}

QUEUE_CONNECTION={Selecciona tu drive, para la prueba se uso database}
```

_4: Migrar tablas y poblar datos por default_

```
php artisan migrate --seed
```

_Por ultimo ejectuta php artisan serve para verque tu proyecto corre bien_

## Ejecucion de Queue ⚙️

Una vez tengamos nuestro proyecto instalado y configurado, podemos empezar a correr nuestros jobs con el siguiente comando:

```
php artisan queue:work database --queue=payment,default
```

De esta forma los jobs registrados se ejecutaran

## Autor ✒️

* **Otoniel Marquez** - *Desarrollador* - [otonielmax](https://gitlab.com/otonielmax)
