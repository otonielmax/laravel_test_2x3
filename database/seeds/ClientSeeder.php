<?php

use App\Client;
use Illuminate\Database\Seeder;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Client::create(['email' => 'juangonzalez@gmail.com', 'join_date' => '2012-02-24']);
        Client::create(['email' => 'ramonvaldez@gmail.com', 'join_date' => '2015-04-01']);
        Client::create(['email' => 'florindamesa@gmail.com', 'join_date' => '2013-10-10']);
        Client::create(['email' => 'miguelmontalvo@gmail.com', 'join_date' => '2014-02-24']);
        Client::create(['email' => 'josejose@gmail.com', 'join_date' => '2012-02-24']);
        Client::create(['email' => 'luissantos@gmail.com', 'join_date' => '2012-06-24']);
        Client::create(['email' => 'ing.marquez05@gmail.com', 'join_date' => '2020-03-01']);
        Client::create(['email' => 'flornuñez@gmail.com', 'join_date' => '2012-02-24']);
        Client::create(['email' => 'kimberlycastro@gmail.com', 'join_date' => '2012-02-24']);
        Client::create(['email' => 'carlosbeltran@gmail.com', 'join_date' => '2012-02-24']);
    }
}
