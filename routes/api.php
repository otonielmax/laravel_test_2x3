<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([], function() {
    // Clients
    Route::get('clients ', 'ClientController@getAllData');

    // Payments
    Route::get('payments ', 'PaymentController@getAllData');
    Route::post('payments', 'PaymentController@store');    
});
